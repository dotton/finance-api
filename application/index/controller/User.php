<?php
namespace app\index\controller;
use \think\Request;

class User {
	public function login() {
		$openid = Request::instance()->post('openid');
		// 判断是否已经注册
		$user = \app\index\model\User::get(['openid' => $openid]);
		if (!empty($user)) {
			$response = [
				'code' => 200,
				'msg' => '登录成功',
				'data' => $user
			];
			return $response;
		}
		//插入一行数据
		$user = new \app\index\model\User();
		// 由于没有内测号，拿不到openid，用res.nickname代替
		$user->openid = $openid;
		// 用户名即昵称
		$user->username = $openid;
		// 生成一个accessToken
		$user->accessToken = md5(time().'mysalt');
		if ($user->save()) {
			$response = [
				'code' => 200,
				'msg' => '注册成功',
				'data' => $user
			];
		}
		// 如果配置了返回为json不必经过toArray()处理后，就能得到一个是纯净的数组，不然是一个对象，包含了诸多信息
		return $response;
	}
}

