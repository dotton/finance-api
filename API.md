#微信小程序开发记账应用实战服务端之用户注册与登录基于ThinkPHP5描述

#1.下载ThinkPHP 5.0.1：http://www.thinkphp.cn/down/855.html

#2.安装 解压到站点根目录

http://localhost/thinkphp_5.0.1_core/public/访问，可以看到

:)
ThinkPHP V5
十年磨一剑 - 为API开发设计的高性能框架
[ V5.0 版本由 七牛云 独家赞助发布 ]
新手快速入门完全开发手册

说明安装成功。

以上url等同于

http://localhost/thinkphp_5.0.1_core/public/index.php/index/index/index

第一个index是模块名，第二个index是控制器名，第三个index是方法名

出处参见config.php Line 60-67

```
    // 默认模块名
    'default_module'         => 'index',
    // 禁止访问模块
    'deny_module_list'       => ['common'],
    // 默认控制器名
    'default_controller'     => 'Index',
    // 默认操作名
    'default_action'         => 'index',
```

#3.用户登录控制器

目标是我们需要一个如下的接口，供我们提交用户信息

http://localhost/thinkphp_5.0.1_core/public/index.php/index/user/login

参数：openid

创建Controller

```
<?php
namespace app\index\controller;

class User {
	public function login() {
		return 'login';
	}
}
```

稍加改造

```
	public function login() {
		header("Content-type: application/json");
		$response = [
			'code' => 200,
			'msg' => '登录成功',
			'data' => ['accessToken' => '14c4b06b824ec593239362517f538b29']
		];
		return json_encode($response);
	}
```

输出json格式

header("Content-type: application/json");

无效！

查看文档，需要改配置

修改第36行

```
// 默认输出类型
'default_return_type'    => 'json',
```

#4.用户登录模型


```
<?php
namespace app\index\model;
use think\Model;

class User extends Model{
	
}
```

#5. 配置数据库信息

进入database.php

```
return [
    // 数据库类型
    'type'           => 'mysql',
    // 服务器地址
    'hostname'       => '127.0.0.1',
    // 数据库名
    'database'       => '',
    // 用户名
    'username'       => 'root',
    // 密码
    'password'       => '',
];

```

#6. 建表

```
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `openid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `accessToken` char(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
```

#7. 获取用户输入

```
// 引入命名空间
use \think\Request;

$openid = Request::instance()->post('openid');
echo $openid;

```

使用PostMan 测试

输出"灵犀网络"字样

![图1](https://static.oschina.net/uploads/img/201610/19213008_9v6N.png "Post取值")

#8. 查询是否用户是否已经存在

通过openid查询数据库，判断用户是否已经注册

如有，返回用户的id,accessToken,username

```
		// 判断是否已经注册
		$user = \app\index\model\User::get(['openid' => $openid]);
		if (!empty($user)) {
			$response = [
				'code' => 200,
				'msg' => '登录成功',
				'data' => $user
			];
			return $response;
		}
```

#9. 插入新用户

创建并返回用户信息

```
		//插入一行数据
		$user = new \app\index\model\User();
		// 由于没有内测号，拿不到openid，用res.nickname代替
		$user->openid = $openid;
		// 用户名即昵称
		$user->username = $openid;
		// 生成一个accessToken
		$user->accessToken = md5(time().'mysalt');
		if ($user->save()) {
			$response = [
				'code' => 200,
				'msg' => '注册成功',
				'data' => $user
			];
		}
		// 如果配置了返回为json不必经过toArray()处理后，就能得到一个是纯净的数组，不然是一个对象，包含了诸多信息
		return $response;
```

#10. json输入用户信息，供小程序本地缓存，主要是accessToken

配置文件config.php指定了返回类型是json的话，不必通过$user->toArray()，即为纯净的array。而不是如下的对象格式。

```
object(app\index\model\User)#4 (27) {
  ["connection":protected]=>
  array(0) {
  }
  ["query":protected]=>
  NULL
  ["name":protected]=>
  string(4) "User"
  ["table":protected]=>
  NULL
  ["class":protected]=>
  string(20) "app\index\model\User"
  ["error":protected]=>
  NULL
  ["validate":protected]=>
  NULL
  ["pk":protected]=>
  NULL
  ["field":protected]=>
  array(0) {
  }
  ["readonly":protected]=>
  array(0) {
  }
  ["visible":protected]=>
  array(0) {
  }
  ["hidden":protected]=>
  array(0) {
  }
  ["append":protected]=>
  array(0) {
  }
  ["data":protected]=>
  array(3) {
    ["openid"]=>
    string(14) "灵犀网络12"
    ["username"]=>
    string(14) "灵犀网络12"
    ["accessToken"]=>
    string(32) "b6c643a133c531728d9829db0d756eaf"
  }
  ["change":protected]=>
  array(3) {
    [0]=>
    string(6) "openid"
    [1]=>
    string(8) "username"
    [2]=>
    string(11) "accessToken"
  }
  ["auto":protected]=>
  array(0) {
  }
  ["insert":protected]=>
  array(0) {
  }
  ["update":protected]=>
  array(0) {
  }
  ["autoWriteTimestamp":protected]=>
  bool(false)
  ["createTime":protected]=>
  string(11) "create_time"
  ["updateTime":protected]=>
  string(11) "update_time"
  ["dateFormat":protected]=>
  string(11) "Y-m-d H:i:s"
  ["type":protected]=>
  array(0) {
  }
  ["isUpdate":protected]=>
  bool(false)
  ["updateWhere":protected]=>
  NULL
  ["relation":protected]=>
  NULL
  ["failException":protected]=>
  bool(false)
}
```

![图2](https://static.oschina.net/uploads/img/201610/19213028_cd3Y.png "登录返回值")

源码下载：关注下方的公众号->回复数字1008

对小程序开发有趣的朋友关注公众号: huangxiujie85，QQ群: 575136499，微信: small_application，陆续还将推出更多作品。

![公众号](https://static.oschina.net/uploads/img/201610/07111145_qD6d.jpg "二维码")
